<?php


	// Extended cpts library
	require __DIR__ . '/includes/cpts/extended-cpts/extended-cpts.php';
	// require __DIR__ . '/includes/cpts/extended-taxos.php';
	require __DIR__ . "/includes/cpts/cpts.php";

	// Wordpress hacks
	require __DIR__ . "/includes/wp-hacks.php";

	// Image sizes
	require __DIR__ . "/includes/image-sizes.php";

	// wp-admin hacks
	require __DIR__ . "/includes/admin.php";

	// theme hacks
	require __DIR__ . "/includes/theme.php";

	// Admin bar
	require __DIR__ . "/includes/adminbar/admin.bar.class.php";
