<!DOCTYPE html>
<html class="no-js" lang="is">
	<?php //$theme = new Theme();	 ?>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="content-type" content="text/html;charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="initial-scale=1, maximum-scale=1">

		<title><?php global $page, $paged; wp_title( '-', true, 'right' ); bloginfo( 'name' ); if ( $paged >= 2 || $page >= 2 ) echo ' - ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) ); ?></title>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/css/swiper.min.css">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/js/swiper.js"></script>

		<?php

			//$theme->metaTags();
			wp_head();

		?>

	</head>

	<body <?php body_class(); ?>>
