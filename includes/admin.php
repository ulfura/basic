<?php

   //allow extra file types uploaded
   add_filter('upload_mimes', 'custom_upload_mimes');
   function custom_upload_mimes ( $existing_mimes=array() ) {
      // add your extension to the mimes array as below
      $existing_mimes['zip'] = 'application/zip';
      $existing_mimes['gz'] = 'application/x-gzip';
      $existing_mimes['svg'] = 'application/svg';
      return $existing_mimes;
   }
 ?>
