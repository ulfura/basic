<?php

  class bbAdminBar {

    private $settings;

    function __construct() {

      if(!is_user_logged_in()) {
        return false;
      }

      $this->settings();

      if($this->settings->hideDefaultAdminBar) {
        add_filter('show_admin_bar', '__return_false');
      }


      add_action( 'wp_footer', array($this, "bar") );
      add_action( 'wp_enqueue_scripts', array($this, "scripts") );

    }

    public function bar() {

      global $post;

      $html = '<div class="bb_adminbar">';

      $html .= '<ul class="default">';

      if(current_user_can($this->settings->role)) {
        $html .= '<li><a href="#" class="js-toggle-grid js-trigger">Grid</a></li>';
      }

      if(is_singular()) {

        $html .= '<li><a href="'.admin_url('post.php?post='.$post->ID.'&action=edit').'" class="js-trigger">Breyta</a></li>';

      }

      if(current_user_can($this->settings->role)) {
        $html .= '<li class="js-dev-breakpoint">Breakpoint: <span></span></li>';
      }

      $html .= '</ul>';

      $html .= '</div>';

      $html .= $this->grid();

      echo $html;

    }

    public function scripts() {

      wp_enqueue_style( 'bb-style-dev', get_template_directory_uri() . '/includes/adminbar/css/development.css', array(), '1.0.0', false );
      wp_enqueue_script( 'bb-js-dev', get_template_directory_uri() . '/includes/adminbar/assets/min/development-min.js', array(), '1.0.0', true );

    }

    public function grid() {

      $output = '<div id="grid-displayer" style="z-index: 99999;">';

      $output .= '<div class="gd-container container">';
      $output .= '<div class="gd-row row">';

      foreach(range(1, $this->settings->grid->columns) as $column) {

        $output .= '<div class="gd-column small-1 columns">';
        $output .= '<div class="gd-column-inside">&nbsp;</div>';
        $output .= '</div>';

      }

      $output .= '</div>';
      $output .= '</div>';
      $output .= '</div>';

      if(current_user_can($this->settings->role)) {

        return $output;

      }

    }

    private function settings() {

      $this->settings = (object) array(

        "role" => "administrator",
        "hideDefaultAdminBar" => true,
        "grid" => (object) array(

          "show" => false,
          "columns" => 12

        )

      );


    }

  }

  new bbAdminBar();

?>
