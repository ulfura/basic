// @codekit-prepend "jquery.cookie.js";

$( document ).ready(function() {

  if($.cookie('grid')) {
    $("#grid-displayer").addClass("show");
  }

  $(".js-dev-breakpoint span").html(Foundation.MediaQuery.current);

  $(document).on("click", ".js-toggle-grid", function(e) {

    e.preventDefault();

    showHideGrid();
  });

  $(window).on('changed.zf.mediaquery', function(event, newSize, oldSize){

    $(".js-dev-breakpoint span").html(newSize);

  });

  $(document).on('keydown', function(e) {

    if ( !$('input:focus').length ) {

      if(e.keyCode == 68) {

        $(".bb_adminbar").toggleClass("hide");

      }

      if(e.keyCode == 71) {

        showHideGrid();

      }

    }


  });

});

function showHideGrid() {

  var el = $("#grid-displayer");

  if(el.hasClass("show")) {

    el.removeClass("show");
    $.removeCookie('grid');

  } else {

    el.addClass("show");

    $.cookie('grid', 'show');

  }

}
