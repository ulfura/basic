<?php

	/*
		Custom post types
	*/

	if(function_exists( 'register_extended_post_type' )) {

		register_extended_post_type( 'locations', array(
			'hierarchical' => true,
			'quick_edit' => false,
			'menu_position' => 20,
			'show_in_rest' => true,
			),
			array(

			   # Override the base names used for labels:
			   'singular' => 'Location',
			   'plural'   => 'Locations',
			   'slug'     => 'stadir'

			)
		);

		add_post_type_support( 'locations', 'revisions' );
		add_post_type_support( 'locations', 'page-attributes' );


	}

?>
