<?php

	class Theme {

		private $settings = array();

		function __construct() {

			// Get manifest.js settings
			$this->getThemeSettings();

		}

		public function getThemeSettings() {

			$json_path = get_template_directory() . '/manifest.json';
			$json = file_get_contents($json_path);

			$this->settings = json_decode($json);
		}

		public function includeScripts() {

			global $post;

			// CSS
			wp_enqueue_style( 'stylesheet', get_template_directory_uri() . '/assets/css/style.css', array(), $this->settings->assets_version, false );

			// JS
			wp_enqueue_script( 'js-libs', get_template_directory_uri() . '/assets/js/script-min.js', array(), $this->settings->assets_version, true );

			wp_localize_script(
				'js-libs',
				'theme',
				$this->themeLocalizeSettings()
			);

		}

		public function themeLocalizeSettings() {
      // helpful for ajax loads to get info about where u are
			global $post;

			$settings = array();
			$settings["logged_in"] = 0;

			if(is_user_logged_in()) {
				$settings["logged_in"] = 1;
			}

			$settings["singular"] = is_singular();
			$settings["post_id"] = $post->ID;

			$settings["site_id"] = get_current_blog_id();
			$settings["site_url"] = site_url();
			$settings["site"] =  WP_ENV;

			return $settings;

		}

		public function disableHeadInits() {

			// Remove the REST API endpoint.
	    remove_action('rest_api_init', 'wp_oembed_register_route');

	    // Turn off oEmbed auto discovery.
	    // Don't filter oEmbed results.
	    remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

	    // Remove oEmbed discovery links.
	    remove_action('wp_head', 'wp_oembed_add_discovery_links');

	    // Remove oEmbed-specific JavaScript from the front-end and back-end.
	    remove_action('wp_head', 'wp_oembed_add_host_js');

		}

		public function metaTags() {

	      global $post;

	      $fb_img = get_field('og_image');
	      $summary = get_field('og_description');
				$summary = preg_replace( "/\r|\n/", "", $summary );
	      $title = get_field('og_title');

	      echo '<meta property="og:url" content="'.get_permalink().'" /> '."\n";

	      echo '<meta property="og:type" content="website" />'."\n";

				if(!empty($title)) {
		      echo '<meta property="og:title" content="'.$title.'" />'."\n";
				} else {
					echo '<meta property="og:title" content="'.get_the_title().'" />'."\n";
				}
	      echo '<meta property="og:locale" content="is_IS" />'."\n";

	      if(!empty($summary)) {

	        echo '<meta property="og:description" content="'.$summary.'" />'."\n";

	      } else {

					$summary = strip_tags(get_field('default_og_description', 'option'));
					$summary = preg_replace( "/\r|\n/", "", $summary );
					echo '<meta property="og:description" content="'.$summary.'" />'."\n";

				}

	      $img_width = $fb_img["sizes"]["facebook_image-width"];
	      $img_height = $fb_img["sizes"]["facebook_image-height"];

	      if(!empty($img)) {
	        echo '<meta property="og:image" content="'.$fb_img['url'].'" />'."\n";
	        echo '<meta property="og:image:secure_url" content="'.$fb_img['url'].'" />'."\n";
	        echo '<meta property="og:image:width" content="'.$img_width.'" />'."\n";
	        echo '<meta property="og:image:height" content="'.$img_height.'" />'."\n";
	      }

	      if($summary) {
	        echo '<meta name="description" content="'.$summary.'"/>';
	      }

	  }


	}

	$theme = new Theme();

	// Include CSS and JS
	add_action("wp_enqueue_scripts",        array($theme, "includeScripts"));

	// Disable <head> inits
	add_action("init", 											array($theme, "disableHeadInits"));



?>
