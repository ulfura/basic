<?php

	/*
		Fjarlægja íslenska stafi úr skrám
	*/
	function sanitize_file_name_chars($filename) {

		// http://vaughnroyko.com/better-upload-filename-sanitization-in-wordpress/

		$sanitized_filename = remove_accents($filename); // Convert to ASCII

		// Standard replacements
		$invalid = array(
			' ' => '-',
			'%20' => '-',
			'_' => '-'
		);
		$sanitized_filename = str_replace(array_keys($invalid), array_values($invalid), $sanitized_filename);

		$sanitized_filename = preg_replace('/[^A-Za-z0-9-\. ]/', '', $sanitized_filename); // Remove all non-alphanumeric except .
		$sanitized_filename = preg_replace('/\.(?=.*\.)/', '', $sanitized_filename); // Remove all but last .
		$sanitized_filename = preg_replace('/-+/', '-', $sanitized_filename); // Replace any more than one - in a row
		$sanitized_filename = str_replace('-.', '.', $sanitized_filename); // Remove last - if at the end
		$sanitized_filename = strtolower($sanitized_filename); // Lowercase

		return $sanitized_filename;
	}
	add_filter('sanitize_file_name', 'sanitize_file_name_chars', 10);

	/*
		Fjarlægja drasl úr header
	*/
	function removeHeaderStuff() {
		// Remove stuff from wp_head()
		remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); // Emoji
		remove_action( 'wp_print_styles', 'print_emoji_styles' ); // Emoji css
		remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
		remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
		remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
		remove_action( 'wp_head', 'index_rel_link' ); // index link
		remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
		remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
		remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
		remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version

	}
	add_action( 'wp_enqueue_scripts', 'removeHeaderStuff' );


?>
