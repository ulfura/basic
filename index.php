<?php

global $post;

  include __DIR__ . '/header.php';

  if(is_front_page()) {

    include get_template_directory() . '/views/pages/page-home.php';

  }

  elseif(is_page() && !is_front_page()) {

    include get_template_directory() . '/views/pages/page-default.php';

  }

  else if(is_singular('locations')) {
    include get_template_directory() . '/views/pages/page-location.php';
  }

  elseif(is_404()) {

    include get_template_directory() . '/views/pages/page-404.php';

  }

  include __DIR__ . '/footer.php';

?>
