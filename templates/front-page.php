<?php /* Template Name: Front page
         Template Post Type: page   */
?>
<?php include get_template_directory() . '/header.php';

the_title('<h1>','</h1>');
?>

<div class="swiper-container">
  <div class="swiper-wrapper">

	<?php

		$gallery = get_field('myndir_slider');

		// Loop through each image in gallery
		foreach( $gallery as $img) {
			$image_list .= '<div class="swiper-slide">' . '<img src="' . $img['url'] . '">' . '</div>';
		}
		echo $image_list;
	?>
  </div>
	<div class="swiper-pagination"></div>

</div>


<?php include get_template_directory() . '/footer.php'; ?>
