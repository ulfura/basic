<?php /* Template Name: Location page
         Template Post Type: page      */
?>

<?php

  include get_template_directory() . '/header.php';

  the_title('<h3>','</h3>');
  while(have_posts()) : the_post();
    the_content();
  endwhile;

  $today = date('Y-m-d');
  $posts_per_page = 50;

  $args = array(
    "post_type" => "locations",
    "post_parent" => 0,
    "posts_per_page" => $posts_per_page,
    'post_status' => 'publish',
    'orderby' => array(
      'location' => 'ASC',
    )
  );

  $the_query = new WP_Query( $args );

  if ($the_query->have_posts()) {
?>

  <div class="section__locations">

    <?php
      while ( $the_query->have_posts() ) {
        $the_query->the_post();

        include get_template_directory() . '/views/pages/page-location.php';

      }
      wp_reset_postdata();
    ?>

  </div>

<?php

}
include get_template_directory() . '/footer.php';

?>
